package observatory

import scala.collection.concurrent.TrieMap
import org.junit.Assert._
import org.junit.Test

trait InteractionTest extends MilestoneSuite {
  private val milestoneTest = namedMilestoneTest("interactive visualization", 3) _

  @Test def testGenerateTilesGenerateAllTiles(): Unit = {
    @volatile var temp: List[(Year, Tile)] = List.empty[(Year, Tile)]
    def mockWriter[Data]: (Year, Tile, Data) => Unit = (year: Year, aTile: Tile, data: Data) => data match {
      case _ => temp = (year, aTile) +: temp
    }
    val testData: Iterable[(Year, None.type)] = for (i <- 1975 to 2015) yield (i, None)
    Interaction.generateTiles(testData, mockWriter)

    Thread.sleep(2000)

    val result: Map[Year, List[Tile]] = temp.groupBy(_._1).mapValues(_.map(_._2))

    result.foreach({ case (_: Year, tiles: List[Tile]) =>
      val tilesByZoom = tiles.groupBy(_.zoom)
      for (z <- tilesByZoom) {
        println(s"zoom: ${z._1} => ${z._2.sortBy(_.y).sortBy(_.x).sortBy(_.zoom)}")
        val errMessage = s"zoom: ${z._1} - Expected ${scala.math.pow(2, 2 * z._1).toInt} tiles, got ${z._2.size} tiles"
        assertEquals(errMessage, scala.math.pow(2, 2 * z._1).toInt, z._2.size)
      }
    })
  }
}
