package observatory

object Main extends App {

  def prepareTemperatureData(year: Year): (Year, Iterable[(Location, Temperature)]) = {
    val temperatureData = Extraction.locateTemperatures(year, "/stations.csv", s"/${year}.csv")
    (year, Extraction.locationYearlyAverageRecords(temperatureData))
  }

  println("Reading files...")
  val data = (for (y <- 1975 to 1976) yield  y).map({y =>
    task(prepareTemperatureData(y))
  }).map(t => t.join()).toList

  println("Done!")
  println("Generating tiles...")
  Interaction.generateTiles(data, Interaction.write)
  println("Done generating tiles")
}
